using Steamworks;
using System;
using System.Globalization;
using uMod.Auth;
using uMod.Common;
using uMod.Text;
using uMod.Unity;
using UnityEngine;

namespace uMod.Game.Hurtworld
{
    /// <summary>
    /// Represents a player, either connected or not
    /// </summary>
    public class HurtworldPlayer : UniversalPlayer, IPlayer
    {
        #region Initialization

        private readonly CSteamID cSteamId;
        private readonly ulong steamId;

        internal PlayerSession session;

        public HurtworldPlayer(string playerId, string playerName)
        {
            // Store player details
            Id = playerId;
            Name = playerName.Sanitize();
            ulong.TryParse(playerId, out steamId);
        }

        public HurtworldPlayer(PlayerSession session) : this(session.SteamId.m_SteamID.ToString(), session.Identity.Name)
        {
            // Store player objects
            this.session = session;
            cSteamId = session.SteamId;
        }

        #endregion Initialization

        #region Objects

        /// <summary>
        /// Gets the object that backs the player
        /// </summary>
        public object Object { get; set; }

        /// <summary>
        /// Gets the player's last command type
        /// </summary>
        public CommandType LastCommand { get; set; }

        /// <summary>
        /// Reconnects the gamePlayer to the player object
        /// </summary>
        /// <param name="gamePlayer"></param>
        public void Reconnect(object gamePlayer)
        {
            // Reconnect player objects
            Object = gamePlayer;
            session = gamePlayer as PlayerSession;
        }

        #endregion Objects

        #region Information

        /// <summary>
        /// Gets/sets the name for the player
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets the ID for the player (unique within the current game)
        /// </summary>
        public override string Id { get; }

        /// <summary>
        /// Gets the language for the player
        /// </summary>
        public CultureInfo Language
        {
            get
            {
                return CultureInfo.GetCultureInfo(session?.WorldPlayerEntity?.PlayerOptions?.CurrentConfig.CurrentLanguage ?? "en");
            }
        }

        /// <summary>
        /// Gets the IP address for the player
        /// </summary>
        public string Address => session?.Player.ipAddress ?? string.Empty;

        /// <summary>
        /// Gets the average network ping for the player
        /// </summary>
        public int Ping => session?.Player.averagePing ?? 0;

        /// <summary>
        /// Gets if the player is a server admin
        /// </summary>
        public override bool IsAdmin => GameManager.Instance.IsAdmin(cSteamId);

        /// <summary>
        /// Gets if the player is a server moderator
        /// </summary>
        public override bool IsModerator => IsAdmin;

        /// <summary>
        /// Gets if the player is banned
        /// </summary>
        public bool IsBanned => BanManager.Instance.IsBanned(steamId);

        /// <summary>
        /// Gets if the player is connected
        /// </summary>
        public bool IsConnected => session?.Player.isConnected ?? false;

        /// <summary>
        /// Gets if the player is alive
        /// </summary>
        public bool IsAlive => !IsDead;

        /// <summary>
        /// Gets if the player is dead
        /// </summary>
        public bool IsDead => session?.WorldPlayerEntity?.PlayerStatManager?.Dead ?? false;

        /// <summary>
        /// Gets if the player is sleeping
        /// </summary>
        public bool IsSleeping => session?.Identity?.Sleeper != null;

        /// <summary>
        /// Gets if the player is the server
        /// </summary>
        public bool IsServer => false;

        /// <summary>
        /// Gets/sets if the player has connected before
        /// </summary>
        public bool IsReturningPlayer { get; set; }

        /// <summary>
        /// Gets/sets if the player has spawned before
        /// </summary>
        public bool HasSpawnedBefore { get; set; }

        #endregion Information

        #region Administration

        /// <summary>
        /// Bans the player for the specified reason and duration
        /// </summary>
        /// <param name="reason"></param>
        /// <param name="duration"></param>
        public void Ban(string reason, TimeSpan duration = default)
        {
            // Check if already banned
            if (!IsBanned)
            {
                // Ban player
                BanManager.Instance.AddBan(steamId);

                // Kick player with reason
                Kick(reason);
            }
        }

        /// <summary>
        /// Gets the amount of time remaining on the player's ban
        /// </summary>
        public TimeSpan BanTimeRemaining => IsBanned ? TimeSpan.MaxValue : TimeSpan.Zero;

        /// <summary>
        /// Kicks the player from the server
        /// </summary>
        /// <param name="reason"></param>
        public void Kick(string reason = "")
        {
            if (IsConnected)
            {
                GameManager.Instance.KickPlayer(session, reason);
            }
        }

        /// <summary>
        /// Unbans the player
        /// </summary>
        public void Unban()
        {
            // Check if already unbanned
            if (IsBanned)
            {
                // Unban player
                BanManager.Instance.RemoveBan(steamId);
            }
        }

        #endregion Administration

        #region Character

        /// <summary>
        /// Gets/sets the player's health
        /// </summary>
        public float Health
        {
            get
            {
                EntityStats stats = session?.WorldPlayerEntity?.GetComponent<EntityStats>();
                return stats?.GetFluidEffect(EntityFluidEffectKeyDatabase.Instance.Health).GetValue() ?? 0f;
            }
            set
            {
                if (session?.WorldPlayerEntity != null)
                {
                    EntityEffectFluid effect = new EntityEffectFluid(EntityFluidEffectKeyDatabase.Instance.Health, EEntityEffectFluidModifierType.SetValuePure, value);
                    effect.Apply(session.WorldPlayerEntity.GetComponent<EntityStats>());
                }
            }
        }

        /// <summary>
        /// Gets/sets the player's maximum health
        /// </summary>
        public float MaxHealth
        {
            get
            {
                EntityStats stats = session?.WorldPlayerEntity?.GetComponent<EntityStats>();
                return stats?.GetFluidEffect(EntityFluidEffectKeyDatabase.Instance.Health).GetMaxValue() ?? 0f;
            }
            set
            {
                EntityStats stats = session.WorldPlayerEntity.GetComponent<EntityStats>();
                if (stats?.GetFluidEffect(EntityFluidEffectKeyDatabase.Instance.Health) is StandardEntityFluidEffect effect)
                {
                    effect.MaxValue = value;
                }
            }
        }

        /// <summary>
        /// Heals the player's character by specified amount
        /// </summary>
        /// <param name="amount"></param>
        public void Heal(float amount)
        {
            if (session?.WorldPlayerEntity != null)
            {
                EntityEffectFluid effect = new EntityEffectFluid(EntityFluidEffectKeyDatabase.Instance.Health, EEntityEffectFluidModifierType.AddValuePure, amount);
                effect.Apply(session.WorldPlayerEntity.GetComponent<EntityStats>());
            }
        }

        /// <summary>
        /// Damages the player's character by specified amount
        /// </summary>
        /// <param name="amount"></param>
        public void Hurt(float amount)
        {
            if (session?.WorldPlayerEntity != null)
            {
                EntityEffectFluid effect = new EntityEffectFluid(EntityFluidEffectKeyDatabase.Instance.Damage, EEntityEffectFluidModifierType.AddValuePure, -amount);
                effect.Apply(session.WorldPlayerEntity.GetComponent<EntityStats>());
            }
        }

        /// <summary>
        /// Causes the player's character to die
        /// </summary>
        public void Kill()
        {
            if (session?.WorldPlayerEntity != null)
            {
                EntityStats stats = session.WorldPlayerEntity.GetComponent<EntityStats>();
                EntityEffectSourceData entityEffectSourceDatum = new EntityEffectSourceData { SourceDescriptionKey = "EntityStats/Sources/Suicide" };
                stats?.HandleEvent(new EntityEventDataRaiseEvent { EventType = EEntityEventType.Die }, entityEffectSourceDatum);
            }
        }

        /// <summary>
        /// Renames the player to specified name
        /// <param name="newName"></param>
        /// </summary>
        public void Rename(string newName)
        {
            if (session != null)
            {
                // Clean up and set if empty
                newName = ChatManagerServer.CleanupGeneral(newName);
                if (string.IsNullOrEmpty(newName.Trim()))
                {
                    newName = "Unnamed"; // TODO: Localization
                }

                // Update name in-game
                session.Identity.Name = newName;
                session.WorldPlayerEntity?.RPC("UpdateName", uLink.RPCMode.OthersExceptOwnerBuffered, newName);

                // Update name in Steam
                SteamGameServer.BUpdateUserData(cSteamId, newName, 0);

                // Update name in uMod
                session.IPlayer.Name = newName;
            }
        }

        /// <summary>
        /// Resets the player's character stats
        /// </summary>
        public void Reset()
        {
            throw new NotImplementedException(); // TODO: Implement when possible
        }

        /// <summary>
        /// Respawns the player's character
        /// </summary>
        public void Respawn()
        {
            throw new NotImplementedException(); // TODO: Implement when possible
        }

        /// <summary>
        /// Respawns the player's character at specified position
        /// </summary>
        public void Respawn(Position pos)
        {
            throw new NotImplementedException(); // TODO: Implement when possible
        }

        #endregion Character

        #region Positional

        /// <summary>
        /// Gets the position of the player
        /// </summary>
        /// <returns></returns>
        public Position Position() => session?.WorldPlayerEntity?.transform.position.ToPosition() ?? new Position();

        /// <summary>
        /// Gets the position of the player
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public void Position(out float x, out float y, out float z)
        {
            Position pos = Position();
            x = pos.X;
            y = pos.Y;
            z = pos.Z;
        }

        /// <summary>
        /// Teleports the player's character to the specified position
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public void Teleport(float x, float y, float z)
        {
            if (IsConnected && session?.WorldPlayerEntity != null)
            {
                session.WorldPlayerEntity.transform.position = new Vector3(x, y, z);
            }
        }

        /// <summary>
        /// Teleports the player's character to the specified position
        /// </summary>
        /// <param name="pos"></param>
        public void Teleport(Position pos) => Teleport(pos.X, pos.Y, pos.Z);

        #endregion Positional

        #region Chat and Commands

        /// <summary>
        /// Sends the specified message and prefix to the player
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Message(string message, string prefix, params object[] args)
        {
            if (!string.IsNullOrEmpty(message) && IsConnected)
            {
                message = args.Length > 0 ? string.Format(Formatter.ToUnity(message), args) : Formatter.ToUnity(message);
                ChatManagerServer.Instance.SendChatMessage(new ServerChatMessage(prefix != null ? $"{prefix} {message}" : message, false), session.Player);
            }
        }

        /// <summary>
        /// Sends the specified message to the player
        /// </summary>
        /// <param name="message"></param>
        public void Message(string message) => Message(message, null);

        /// <summary>
        /// Replies to the player with the specified message and prefix
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Reply(string message, string prefix, params object[] args) => Message(message, prefix, args);

        /// <summary>
        /// Replies to the player with the specified message
        /// </summary>
        /// <param name="message"></param>
        public void Reply(string message) => Message(message, null);

        /// <summary>
        /// Runs the specified console command on the player
        /// </summary>
        /// <param name="command"></param>
        /// <param name="args"></param>
        public void Command(string command, params object[] args)
        {
            throw new NotImplementedException(); // TODO: Implement when possible
        }

        #endregion Chat and Commands
    }
}
