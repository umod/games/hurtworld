using System;
using System.Collections.Generic;
using System.Reflection;
using uMod.Common;
using uMod.Extensions;

namespace uMod.Game.Hurtworld
{
    /// <summary>
    /// The extension class that represents this extension
    /// </summary>
    [GameExtension]
    public class HurtworldExtension : Extension
    {
        // Get assembly info
        internal static Assembly Assembly = Assembly.GetExecutingAssembly();
        internal static AssemblyName AssemblyName = Assembly.GetName();
        internal static VersionNumber AssemblyVersion = new VersionNumber(AssemblyName.Version.Major, AssemblyName.Version.Minor, AssemblyName.Version.Build);
        internal static string AssemblyAuthors = ((AssemblyCompanyAttribute)Attribute.GetCustomAttribute(Assembly, typeof(AssemblyCompanyAttribute), false)).Company;

        /// <summary>
        /// Gets whether this extension is for a specific game
        /// </summary>
        public override bool IsGameExtension => true;

        /// <summary>
        /// Gets the title of this extension
        /// </summary>
        public override string Title => "Hurtworld";

        /// <summary>
        /// Gets the author of this extension
        /// </summary>
        public override string Author => AssemblyAuthors;

        /// <summary>
        /// Gets the version of this extension
        /// </summary>
        public override VersionNumber Version => AssemblyVersion;

        /// <summary>
        /// Gets the branch of this extension
        /// </summary>
        public override string Branch => "public"; // TODO: Handle this programmatically

        /// <summary>
        /// Commands that plugins are prevented from overriding
        /// </summary>
        internal static IEnumerable<string> RestrictedCommands => new[]
        {
            "bindip",
            "host",
            "queryport"
        };

        /// <summary>
        /// Default game-specific references for use in plugins
        /// </summary>
        public override string[] DefaultReferences => new[]
        {
            ""
        };

        /// <summary>
        /// List of namespaces allowed for use in plugins
        /// </summary>
        public override string[] WhitelistedNamespaces => new[]
        {
            "uLink"
        };

        /// <summary>
        /// Initializes a new instance of the HurtworldExtension class
        /// </summary>
        public HurtworldExtension()
        {
        }

        /// <summary>
        /// Called when this extension has been added to the specified manager
        /// </summary>
        /// <param name="manager"></param>
        public override void HandleAddedToManager(IExtensionManager manager)
        {
            manager.RegisterPluginLoader(new HurtworldPluginLoader(Interface.uMod.RootLogger));
        }
    }
}
