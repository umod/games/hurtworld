﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Net;
using uMod.Common;
using uMod.Text;
using UnityEngine;
using WebResponse = uMod.Common.Web.WebResponse;

namespace uMod.Game.Hurtworld
{
    /// <summary>
    /// Represents the server hosting the game instance
    /// </summary>
    public class HurtworldServer : IServer
    {
        internal static readonly BanManager BanManager = BanManager.Instance;
        internal static readonly ChatManagerServer ChatManager = ChatManagerServer.Instance;
        internal static readonly ConsoleManager ConsoleManager = ConsoleManager.Instance;

        /// <summary>
        /// Gets or sets the player manager
        /// </summary>
        public IPlayerManager PlayerManager { get; set; }

        #region Server Information

        /// <summary>
        /// Gets/sets the public-facing name of the server
        /// </summary>
        public string Name
        {
            get => GameManager.Instance.ServerConfig.GameName;
            set => GameManager.Instance.ServerConfig.GameName = value;
        }

        private static IPAddress address;
        private static IPAddress localAddress;

        /// <summary>
        /// Gets the public-facing IP address of the server, if known
        /// </summary>
        public IPAddress Address
        {
            get
            {
                try
                {
                    if (address == null || !Utility.ValidateIPv4(address.ToString()))
                    {
                        if (Utility.ValidateIPv4(GameManager.Instance.ServerConfig.BoundIP) && !Utility.IsLocalIP(GameManager.Instance.ServerConfig.BoundIP))
                        {
                            IPAddress.TryParse(GameManager.Instance.ServerConfig.BoundIP, out address);
                            Interface.uMod.LogInfo($"IP address from server: {address}"); // TODO: Localization
                        }
                        else
                        {
                            Web.Client webClient = new Web.Client();
                            webClient.Get("https://api.ipify.org")
                               .Done(delegate (WebResponse response)
                               {
                                   if (response.StatusCode == 200)
                                   {
                                       IPAddress.TryParse(response.ReadAsString(), out address);
                                       Interface.uMod.LogInfo($"IP address from external API: {address}"); // TODO: Localization
                                   }
                               });
                        }
                    }

                    return address;
                }
                catch (Exception ex)
                {
                    Interface.uMod.LogWarning("Couldn't get server's public IP address", ex); // TODO: Localization
                    return IPAddress.Any;
                }
            }
        }

        /// <summary>
        /// Gets the local IP address of the server, if known
        /// </summary>
        public IPAddress LocalAddress
        {
            get
            {
                try
                {
                    return localAddress ?? (localAddress = Utility.GetLocalIP());
                }
                catch
                {
                    return IPAddress.Any;
                }
            }
        }

        /// <summary>
        /// Gets the public-facing network port of the server, if known
        /// </summary>
        public ushort Port => (ushort)GameManager.Instance.ServerConfig.Port;

        /// <summary>
        /// Gets the version or build number of the server
        /// </summary>
        public string Version => GameManager.Instance.Version.ToString();

        /// <summary>
        /// Gets the network protocol version of the server
        /// </summary>
        public string Protocol => GameManager.PROTOCOL_VERSION.ToString();

        /// <summary>
        /// Gets the language set by the server
        /// </summary>
        public CultureInfo Language => CultureInfo.InstalledUICulture;

        /// <summary>
        /// Gets the total of players currently on the server
        /// </summary>
        public int Players => GameManager.Instance.GetPlayerCount();

        /// <summary>
        /// Gets/sets the maximum players allowed on the server
        /// </summary>
        public int MaxPlayers
        {
            get => GameManager.Instance.ServerConfig.MaxPlayers;
            set => GameManager.Instance.ServerConfig.MaxPlayers = value;
        }

        /// <summary>
        /// Gets/sets the current in-game time on the server
        /// </summary>
        public DateTime Time
        {
            get
            {
                GameTime time = TimeManager.Instance.GetCurrentGameTime();
                return Convert.ToDateTime($"{time.Hour}:{time.Minute}:{Math.Floor(time.Second)}");
            }
            set
            {
                double currentOffset = TimeManager.Instance.GetCurrentGameTime().offset;
                int daysPassed = TimeManager.Instance.GetCurrentGameTime().Day + 1;
                double newOffset = 86400 * daysPassed - currentOffset + value.TimeOfDay.TotalSeconds;
                TimeManager.Instance.InitialTimeOffset += (float)newOffset;
            }
        }

        /// <summary>
        /// Gets the current or average frame rate of the server
        /// </summary>
        public int FrameRate
        {
            get => Mathf.RoundToInt(1f / UnityEngine.Time.smoothDeltaTime);
        }

        /// <summary>
        /// Gets/sets the target frame rate for the server
        /// </summary>
        public int TargetFrameRate
        {
            get => UnityEngine.Application.targetFrameRate;
            set => uLinkTargetFrameRateFix.SetTargetFrameRate(value);
        }

        /// <summary>
        /// Gets information on the currently loaded save file
        /// </summary>
        ISaveInfo IServer.SaveInfo => throw new NotImplementedException(); // TODO: Implement when possible

        #endregion Server Information

        #region Server Administration

        /// <summary>
        /// Saves the server and any related information
        /// </summary>
        public void Save()
        {
            BanManager.Instance.Save();
            ConsoleManager.StartCoroutine(GameSerializer.Instance.SaveServer(string.Concat("autosave_", Singleton<GameManager>.Instance.ServerConfig.Map), true));
        }

        /// <summary>
        /// Shuts down the server, with optional saving and delay
        /// </summary>
        public void Shutdown(bool save = true, int delay = 0)
        {
            if (!GameSerializer.Instance.IsSaving())
            {
                if (save)
                {
                    Save(); // TODO: Check to make sure this is not already done on quit
                }

                GameManager.Instance.Quit(delay);
                UnityEngine.Application.Quit(); // TODO: Might be necessary, process does not seem to always close
                Process.GetCurrentProcess().Kill(); // TODO: Might be necessary, process does not seem to always close
            }
        }

        #endregion Server Administration

        #region Player Administration

        /// <summary>
        /// Bans the player for the specified reason and duration
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="reason"></param>
        /// <param name="duration"></param>
        public void Ban(string playerId, string reason, TimeSpan duration = default)
        {
            // Check if already banned
            if (!IsBanned(playerId))
            {
                // Ban player
                ulong.TryParse(playerId, out ulong guid);
                BanManager.AddBan(guid);

                // TODO: Implement universal ban storage
            }
        }

        /// <summary>
        /// Gets the amount of time remaining on the player's ban
        /// </summary>
        /// <param name="playerId"></param>
        public TimeSpan BanTimeRemaining(string playerId) => IsBanned(playerId) ? TimeSpan.MaxValue : TimeSpan.Zero;

        /// <summary>
        /// Gets if the player is banned
        /// </summary>
        /// <param name="playerId"></param>
        public bool IsBanned(string playerId)
        {
            ulong.TryParse(playerId, out ulong guid);
            return BanManager.IsBanned(guid);
        }

        /// <summary>
        /// Unbans the player
        /// </summary>
        /// <param name="playerId"></param>
        public void Unban(string playerId)
        {
            // Check if already unbanned
            if (IsBanned(playerId))
            {
                // Unban player
                ulong.TryParse(playerId, out ulong guid);
                BanManager.RemoveBan(guid);

                // TODO: Implement universal ban storage
            }
        }

        #endregion Player Administration

        #region Chat and Commands

        /// <summary>
        /// Broadcasts the specified chat message and prefix to all players
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Broadcast(string message, string prefix, ulong id, params object[] args)
        {
            if (!string.IsNullOrEmpty(message))
            {
                message = args.Length > 0 ? string.Format(Formatter.ToPlaintext(message), args) : Formatter.ToPlaintext(message);
                ChatManager.SendChatMessage(new ServerChatMessage(prefix != null ? $"{prefix}: {message}" : message));
                ConsoleManager.SendLog($"[Chat] {message}");
            }
        }

        /// <summary>
        /// Broadcasts the specified chat message and prefix to all players
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Broadcast(string message, string prefix, params object[] args) => Broadcast(message, prefix, 0uL, args);

        /// <summary>
        /// Broadcasts the specified chat message to all players with the specified icon
        /// </summary>
        /// <param name="message"></param>
        /// <param name="id"></param>
        /// <param name="args"></param>
        public void Broadcast(string message, ulong id, params object[] args) => Broadcast(message, string.Empty, id, args);

        /// <summary>
        /// Broadcasts the specified chat message to all players
        /// </summary>
        /// <param name="message"></param>
        public void Broadcast(string message) => Broadcast(message, null);

        /// <summary>
        /// Runs the specified server command
        /// </summary>
        /// <param name="command"></param>
        /// <param name="args"></param>
        public void Command(string command, params object[] args)
        {
            if (!string.IsNullOrEmpty(command))
            {
                ConsoleManager.ExecuteCommand($"{command} {string.Join(" ", Array.ConvertAll(args, x => x.ToString()))}");
            }
        }

        #endregion Chat and Commands
    }
}
