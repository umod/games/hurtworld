using uMod.Auth;
using uMod.Common;

namespace uMod.Game.Hurtworld
{
    /// <summary>
    /// Represents a Hurtworld player manager
    /// </summary>
    public class HurtworldPlayerManager : PlayerManager<HurtworldPlayer>
    {
        /// <summary>
        /// Create a new instance of the HurtworldPlayerManager class
        /// </summary>
        /// <param name="application"></param>
        /// <param name="logger"></param>
        public HurtworldPlayerManager(IApplication application, ILogger logger) : base(application, logger)
        {
        }
    }
}
