using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using uMod.Common;
using uMod.Configuration;
using uMod.Plugins;
using uMod.Plugins.Decorators;
using UnityEngine;
using NetworkPlayer = uLink.NetworkPlayer;

namespace uMod.Game.Hurtworld
{
    /// <summary>
    /// The core Hurtworld plugin
    /// </summary>
    [HookDecorator(typeof(ServerDecorator))]
    public class Hurtworld : Plugin
    {
        #region Initialization

        internal static readonly HurtworldProvider Universal = HurtworldProvider.Instance;

        /// <summary>
        /// Initializes a new instance of the Hurtworld class
        /// </summary>
        public Hurtworld()
        {
            // Set plugin info attributes
            Title = "Hurtworld";
            Author = HurtworldExtension.AssemblyAuthors;
            Version = HurtworldExtension.AssemblyVersion;
        }

        /// <summary>
        /// Called when the server is first initialized
        /// </summary>
        [Hook("OnServerInitialized")]
        private void OnServerInitialized()
        {
            // Add universal commands
            Universal.CommandSystem.DefaultCommands.Initialize(this);

            // Clean up invalid permission data
            permission.RegisterValidate(ExtensionMethods.IsSteamId);
            permission.CleanUp();

            SteamGameServer.SetGameTags("umod,modded");
        }

        #endregion Initialization

        #region Clan Hooks

        /// <summary>
        /// Called when the player attempts to create a clan
        /// </summary>
        /// <param name="clanName"></param>
        /// <param name="clanTag"></param>
        /// <param name="color"></param>
        /// <param name="description"></param>
        /// <param name="session"></param>
        /// <returns></returns>
        [Hook("IOnClanCreate")]
        private object IOnClanCreate(string clanName, string clanTag, Color color, string description, PlayerSession session)
        {
            object canCreateClan = Interface.CallHook("OnClanCreate", clanName, clanTag, color, description, session);
            if (canCreateClan is string || canCreateClan is bool && !(bool)canCreateClan)
            {
                ClanManager.Instance.RPC("RPCClanCreationError", session.Player, canCreateClan is string ? canCreateClan.ToString() : "Clan creation was denied"); // TODO: Localization
                return false;
            }

            return null;
        }

        #endregion Clan Hooks

        #region Entity Hooks

        /// <summary>
        /// Called when an entity effect is applied
        /// </summary>
        /// <param name="effect"></param>
        /// <param name="sourceData"></param>
        /// <param name="relativeValue"></param>
        [Hook("IOnEntityEffect")]
        private object IOnEntityEffect(StandardEntityFluidEffect effect, EntityEffectSourceData sourceData, float relativeValue)
        {
            if (sourceData == null || effect.ResolveTargetType() != EntityFluidEffectKeyDatabase.Instance?.Health)
            {
                return null;
            }

            EntityStats stats = effect.EntityStats;
            if (stats == null)
            {
                return null;
            }

            float newValue = Mathf.Clamp(effect.Value + relativeValue, effect.MinValue, effect.MaxValue);
            float updatedValue = newValue - effect.Value;
            sourceData.Value = updatedValue;

            AIEntity entity = stats.GetComponent<AIEntity>();
            if (entity != null)
            {
                if (updatedValue > 0)
                {
                    return Interface.CallHook("OnEntityHeal", entity, sourceData);
                }
                else if (updatedValue < 0)
                {
                    return Interface.CallHook("OnEntityTakeDamage", entity, sourceData);
                }

                return null;
            }

            HNetworkView networkView = stats.networkView;
            if (networkView != null)
            {
                PlayerSession session = GameManager.Instance.GetSession(stats.networkView.owner);
                if (session != null)
                {
                    if (updatedValue > 0)
                    {
                        return Interface.CallHook("OnPlayerHeal", session, sourceData);
                    }
                    else if (updatedValue < 0)
                    {
                        return Interface.CallHook("OnPlayerTakeDamage", session, sourceData);
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Called when an entity effect is initialized
        /// </summary>
        /// <param name="effect"></param>
        /// <param name="stats"></param>
        [Hook("IOnEntityEffectInitialize")]
        private void IOnEntityEffectInitialize(StandardEntityFluidEffect effect, EntityStats stats)
        {
            effect.EntityStats = stats;
        }

        #endregion Entity Hooks

        #region Event Hooks

        /// <summary>
        /// Called when territory claim messages is broadcasted to chat
        /// </summary>
        /// <param name="territoryMarker"></param>
        /// <param name="clan"></param>
        [Hook("IOnClanTerritoryClaimBroadcast")]
        private void IOnClanTerritoryClaimBroadcast(TerritoryControlMarker territoryMarker, Clan clan)
        {
            if (Interface.CallHook("OnClanTerritoryClaimBroadcast", territoryMarker, clan) == null)
            {
                ChatManagerServer.Instance.SendChatMessage(new ServerChatMessage($"Territory {territoryMarker.Stake.TerritoryName} has been claimed by {clan.ClanName}", Color.magenta, true)); // TODO: Use game localization
            }
        }

        /// <summary>
        /// Called when town event message is broadcasted to chat
        /// </summary>
        /// <param name="townEvent"></param>
        /// <param name="name"></param>
        [Hook("IOnTownEventBroadcast")]
        private void IOnTownEventBroadcast(BaseTownEvent townEvent, string name)
        {
            if (Interface.CallHook("OnTownEventBroadcast", townEvent, name) == null)
            {
                ChatManagerServer.Instance.SendChatMessage(new LocalizedChatMessage(Color.yellow, "UI/Chat/TownEventStartFormat", new string[1]
                {
                    name
                }));
            }
        }

        #endregion Event Hooks

        #region Player Hooks

        /// <summary>
        /// Called when the player is attempting to craft
        /// </summary>
        /// <param name="netPlayer"></param>
        /// <param name="recipe"></param>
        /// <returns></returns>
        [Hook("ICanCraft")]
        private object ICanCraft(Crafter crafter, ICraftable recipe, int count)
        {
            PlayerSession session = Find(crafter.NetworkPlayer);
            return Interface.CallHook("CanCraft", crafter, session, recipe, count);
        }

        /// <summary>
        /// Called when the player craft attempt is initialized
        /// </summary>
        /// <param name="crafter"></param>
        /// <param name="networkPlayer"></param>
        [Hook("ICanCraftInitialize")]
        private void ICanCraftInitialize(Crafter crafter, NetworkPlayer networkPlayer)
        {
            crafter.NetworkPlayer = networkPlayer;
        }

        /// <summary>
        /// Called when the player is attempting to connect
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        [Hook("IOnPlayerApprove")]
        private object IOnPlayerApprove(PlayerSession session)
        {
            session.Identity.Name = session.Identity.Name ?? "Unnamed"; // TODO: Localization
            string playerId = session.SteamId.ToString();
            string ipAddress = session.Player.ipAddress;

            Players.PlayerJoin(playerId, session.Identity.Name);

            // Call pre hook for plugins
            object loginUniversal = Interface.CallHook("CanClientLogin", session.Identity.Name, playerId, ipAddress);
            object loginDeprecated = Interface.CallDeprecatedHook("CanUserLogin", "CanClientLogin", new DateTime(2021, 1, 1), session.Identity.Name, playerId, ipAddress);
            object canLogin = loginUniversal is null ? loginDeprecated : loginUniversal;

            // Can the player log in?
            if (canLogin is string || canLogin is bool loginBlocked && !loginBlocked)
            {
                GameManager.Instance.StartCoroutine(GameManager.Instance.DisconnectPlayerSync(session.Player, canLogin is string ? canLogin.ToString() : "Connection was rejected")); // TODO: Localization
                if (session.IsActiveSlot)
                {
                    session.IsActiveSlot = false;
                    GameManager.Instance._activePlayerCount--;
                }
                if (GameManager.Instance._steamIdSession.ContainsKey(session.SteamId))
                {
                    GameManager.Instance._steamIdSession.Remove(session.SteamId);
                }
                if (GameManager.Instance._playerQueue.Contains(session))
                {
                    GameManager.Instance._playerQueue.Remove(session);
                }
                if (GameManager.Instance._steamIdSession.ContainsKey(session.SteamId))
                {
                    GameManager.Instance._steamIdSession.Remove(session.SteamId);
                }
                int authTicketHash = session.AuthTicketBuffer.ComputeHash();
                if (GameManager.Instance._userTokenMap.ContainsKey(authTicketHash))
                {
                    GameManager.Instance._userTokenMap.Remove(authTicketHash);
                }
                if (GameManager.Instance._playerSessions.ContainsKey(session.Player))
                {
                    GameManager.Instance._playerSessions.Remove(session.Player);
                }
                if (session.Identity.ConnectedSession != session)
                {
                    HNetworkManager.Instance.FinalDestroyPlayerObjects(session.Player);
                    session.Reset();
                    ClassInstancePool.Instance.ReleaseInstanceExplicit(session);
                }
                else
                {
                    session.Identity.WriteFromEntity(false);
                    GameManager.Instance.StartCoroutine(GameManager.Instance.RemovePlayerWorldEntity(session));
                }
                return true;
            }

            GameManager.Instance._playerSessions[session.Player] = session;

            // Call post hook for plugins
            Interface.CallHook("OnPlayerApproved", session.Identity.Name, playerId, ipAddress);
            Interface.CallDeprecatedHook("OnUserApprove", "OnPlayerApproved", new DateTime(2020, 6, 1), session);
            Interface.CallDeprecatedHook("OnUserApproved", "OnPlayerApproved", new DateTime(2021, 1, 1), session.Identity.Name, playerId, ipAddress);

            return null;
        }

        /// <summary>
        /// Called when the player sends a chat message
        /// </summary>
        /// <param name="session"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        [Hook("IOnPlayerChat")]
        private object IOnPlayerChat(PlayerSession session, string message)
        {
            // Call hook for plugins
            object chatUniversal = Interface.CallHook("OnPlayerChat", session.IPlayer, message);
            object chatDeprecated = Interface.CallDeprecatedHook("OnUserChat", "OnPlayerChat", new DateTime(2021, 1, 1), session.IPlayer, message);
            return chatUniversal is null ? chatDeprecated : chatUniversal;
        }

        /// <summary>
        /// Called when the player atempts to claim territory
        /// </summary>
        /// <param name="netPlayer"></param>
        /// <param name="clan"></param>
        /// <param name="point"></param>
        /// <returns></returns>
        [Hook("IOnPlayerClaimTerritory")]
        private object IOnPlayerClaimTerritory(NetworkPlayer netPlayer, Clan clan, int point)
        {
            return Interface.CallHook("OnPlayerClaimTerritory", Find(netPlayer), clan, point);
        }

        /// <summary>
        /// Called when the player has claimed territory
        /// </summary>
        /// <param name="netPlayer"></param>
        /// <param name="clan"></param>
        /// <param name="point"></param>
        /// <returns></returns>
        [Hook("IOnPlayerClaimedTerritory")]
        private void IOnPlayerClaimedTerritory(NetworkPlayer netPlayer, Clan clan, int point)
        {
            Interface.CallHook("OnPlayerClaimedTerritory", Find(netPlayer), clan, point);
        }

        /// <summary>
        /// Called when the player sends a chat command
        /// </summary>
        /// <param name="session"></param>
        /// <param name="command"></param>
        [Hook("IOnPlayerCommand")]
        private object IOnPlayerCommand(PlayerSession session, string command)
        {
            // Handle the command
            if (Universal.CommandSystem.HandleChatMessage(session.IPlayer, command) == CommandState.Completed)
            {
                return true;
            }

            return null;
        }

        /// <summary>
        /// Called when the player has connected
        /// </summary>
        /// <param name="session"></param>
        [Hook("OnPlayerConnected")]
        private void OnPlayerConnected(PlayerSession session)
        {
            if (session != null)
            {
                string playerId = session.SteamId.ToString();

                // Add player to default groups
                GroupProvider defaultGroups = Interface.uMod.Auth.Configuration.Groups;
                if (!permission.UserHasGroup(playerId, defaultGroups.Players))
                {
                    permission.AddUserGroup(playerId, defaultGroups.Players);
                }
                if (session.IsAdmin && !permission.UserHasGroup(playerId, defaultGroups.Administrators))
                {
                    permission.AddUserGroup(playerId, defaultGroups.Administrators);
                }

                Players.PlayerConnected(playerId, session);

                IPlayer player = Players.FindPlayerById(playerId);
                if (player != null)
                {
                    // Set default language for player if not set
                    if (string.IsNullOrEmpty(lang.GetLanguage(playerId)))
                    {
                        lang.SetLanguage(player.Language.TwoLetterISOLanguageName, playerId);
                    }

                    // Set IPlayer object in PlayerSession
                    session.IPlayer = player;

                    // Call hook for plugins
                    Interface.CallHook("OnPlayerConnected", session.IPlayer);
                }
            }
        }

        /// <summary>
        /// Called when the player has disconnected
        /// </summary>
        /// <param name="session"></param>
        [Hook("IOnPlayerDisconnected")]
        private void IOnPlayerDisconnected(PlayerSession session)
        {
            if (session.IsLoaded)
            {
                // Call hook for plugins
                Interface.CallHook("OnPlayerDisconnected", session.IPlayer, "Unknown"); // TODO: Localization

                Players.PlayerDisconnected(session.Identity.SteamId.ToString());
            }
        }

        /// <summary>
        /// Called when the server receives input from the player
        /// </summary>
        /// <param name="netPlayer"></param>
        /// <param name="input"></param>
        [Hook("IOnPlayerInput")]
        private void IOnPlayerInput(NetworkPlayer netPlayer, InputControls input)
        {
            PlayerSession session = Find(netPlayer);
            if (session != null)
            {
                Interface.CallHook("OnPlayerInput", session, input);
            }
        }

        /// <summary>
        /// Called when the player attempts to suicide
        /// </summary>
        /// <param name="netPlayer"></param>
        [Hook("IOnPlayerSuicide")]
        private object IOnPlayerSuicide(NetworkPlayer netPlayer)
        {
            PlayerSession session = Find(netPlayer);
            return session != null ? Interface.CallHook("OnPlayerSuicide", session) : null;
        }

        /// <summary>
        /// Called when the player attempts to suicide
        /// </summary>
        /// <param name="netPlayer"></param>
        [Hook("IOnPlayerVoice")]
        private object IOnPlayerVoice(NetworkPlayer netPlayer)
        {
            PlayerSession session = Find(netPlayer);
            return session != null ? Interface.CallHook("OnPlayerVoice", session) : null;
        }

        /// <summary>
        /// Called when the stats for a sleeper changes
        /// </summary>
        /// <param name="sleeper"></param>
        /// <param name="eventData"></param>
        /// <param name="sourceData"></param>
        [Hook("IOnSleeperStatsChange")]
        private object IOnSleeperStatsChange(SleeperServer sleeper, EntityEventData eventData, EntityEffectSourceData sourceData)
        {
            if (sourceData == null || !(eventData is IEventTypeEventData eventTypeEventData))
            {
                return null;
            }

            if (eventTypeEventData.EventType == EEntityEventType.Damaged)
            {
                // TODO: Implement OnSleeperTakeDamage hook
                return null;
            }
            else if (eventTypeEventData.EventType == EEntityEventType.Die)
            {
                if (sleeper._linkedPlayer != null)
                {
                    return Interface.CallHook("OnSleeperDeath", sleeper._linkedPlayer, sourceData);
                }
            }

            return null;
        }

        #endregion Player Hooks

        #region Server Hooks

        /// <summary>
        /// Called when a console command was run
        /// </summary>
        /// <param name="command"></param>
        /// <param name="session"></param>
        /// <returns></returns>
        [Hook("IOnCommand")]
        private object IOnCommand(string command, PlayerSession session)
        {
            if (session == null)
            {
                // Parse the command and args
                string[] fullCommand = CommandLine.Split(command);
                if (fullCommand.Length >= 1)
                {
                    string cmd = fullCommand[0].ToLower();
                    string[] args = fullCommand.Skip(1).ToArray();

                    // Is the server commanda blocked?
                    if (Interface.CallHook("OnServerCommand", cmd, args) != null)
                    {
                        return true;
                    }
                }
            }

            // Handle the command
            IPlayer player = session != null ? session.IPlayer : Universal.CommandSystem.ConsolePlayer;
            if (Universal.CommandSystem.HandleConsoleMessage(player, command) == CommandState.Completed)
            {
                return true;
            }

            return null;
        }

        #endregion Server Hooks

        #region Structure Hooks

        /// <summary>
        /// Called when a double door is used
        /// </summary>
        /// <param name="door"></param>
        /// <returns></returns>
        [Hook("IOnDoubleDoorUsed")]
        private void IOnDoubleDoorUsed(DoubleDoorServer door)
        {
            NetworkPlayer? player = door.LastUsedBy;
            if (player == null)
            {
                return;
            }

            PlayerSession session = Find((NetworkPlayer)player);
            if (session != null)
            {
                Interface.CallHook("OnDoubleDoorUsed", door, session);
            }
        }

        /// <summary>
        /// Called when a garage door is used
        /// </summary>
        /// <param name="door"></param>
        /// <returns></returns>
        [Hook("IOnGarageDoorUsed")]
        private void IOnGarageDoorUsed(GarageDoorServer door)
        {
            NetworkPlayer? netPlayer = door.LastUsedBy;
            if (netPlayer != null)
            {
                PlayerSession session = Find((NetworkPlayer)netPlayer);
                if (session != null)
                {
                    Interface.CallHook("OnGarageDoorUsed", door, session);
                }
            }
        }

        /// <summary>
        /// Called when a single door is used
        /// </summary>
        /// <param name="door"></param>
        /// <returns></returns>
        [Hook("IOnSingleDoorUsed")]
        private void IOnSingleDoorUsed(DoorSingleServer door)
        {
            NetworkPlayer? player = door.LastUsedBy;
            if (player == null)
            {
                return;
            }

            PlayerSession session = Find((NetworkPlayer)player);
            if (session != null)
            {
                Interface.CallHook("OnSingleDoorUsed", door, session);
            }
        }

        #endregion Structure Hooks

        #region Vehicle Hooks

        /// <summary>
        /// Called when a player tries to enter a vehicle
        /// </summary>
        /// <param name="session"></param>
        /// <param name="go"></param>
        /// <returns></returns>
        [Hook("ICanEnterVehicle")]
        private object ICanEnterVehicle(PlayerSession session, GameObject go)
        {
            return Interface.CallHook("CanEnterVehicle", session, go.GetComponent<VehiclePassenger>());
        }

        /// <summary>
        /// Called when a player tries to exit a vehicle
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        [Hook("ICanExitVehicle")]
        private object ICanExitVehicle(VehiclePassenger vehicle)
        {
            PlayerSession session = Find(vehicle.networkView.owner);
            return session != null ? Interface.CallHook("CanExitVehicle", session, vehicle) : null;
        }

        /// <summary>
        /// Called when a player enters a vehicle
        /// </summary>
        /// <param name="netPlayer"></param>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        [Hook("IOnEnterVehicle")]
        private void IOnEnterVehicle(NetworkPlayer netPlayer, VehiclePassenger vehicle)
        {
            PlayerSession session = Find(netPlayer);
            if (session != null)
            {
                Interface.CallHook("OnEnterVehicle", session, vehicle);
            }
        }

        /// <summary>
        /// Called when a player exits a vehicle
        /// </summary>
        /// <param name="netPlayer"></param>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        [Hook("IOnExitVehicle")]
        private void IOnExitVehicle(NetworkPlayer netPlayer, VehiclePassenger vehicle)
        {
            PlayerSession session = Find(netPlayer);
            if (session != null)
            {
                Interface.CallHook("OnExitVehicle", session, vehicle);
            }
        }

        #endregion Vehicle Hooks

        #region Player Finding

        /// <summary>
        /// Gets the player session using a name, Steam ID, or IP address
        /// </summary>
        /// <param name="nameOrIdOrIp"></param>
        /// <returns></returns>
        public static PlayerSession Find(string nameOrIdOrIp)
        {
            PlayerSession session = null;
            foreach (KeyValuePair<uLink.NetworkPlayer, PlayerSession> s in Sessions)
            {
                if (!nameOrIdOrIp.Equals(s.Value.Identity.Name, StringComparison.OrdinalIgnoreCase) &&
                    !nameOrIdOrIp.Equals(s.Value.SteamId.ToString()) && !nameOrIdOrIp.Equals(s.Key.ipAddress))
                {
                    continue;
                }

                session = s.Value;
                break;
            }
            return session;
        }

        /// <summary>
        /// Gets the player session using a uLink.NetworkPlayer
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
        public static PlayerSession Find(uLink.NetworkPlayer player) => GameManager.Instance.GetSession(player);

        /// <summary>
        /// Gets the player session using a UnityEngine.Collider
        /// </summary>
        /// <param name="col"></param>
        /// <returns></returns>
        public static PlayerSession Find(Collider col)
        {
            PlayerSession session = null;
            EntityStats stats = col.gameObject.GetComponent<EntityStatsTriggerProxy>().Stats;
            foreach (KeyValuePair<uLink.NetworkPlayer, PlayerSession> s in Sessions)
            {
                if (!s.Value.WorldPlayerEntity.GetComponent<EntityStats>() == stats)
                {
                    continue;
                }

                session = s.Value;
                break;
            }
            return session;
        }

        /// <summary>
        /// Gets the player session using a UnityEngine.GameObject
        /// </summary>
        /// <param name="go"></param>
        /// <returns></returns>
        public static PlayerSession Find(GameObject go)
        {
            Dictionary<uLink.NetworkPlayer, PlayerSession> sessions = GameManager.Instance.GetSessions();
            return (from i in sessions where go.Equals(i.Value.WorldPlayerEntity) select i.Value).FirstOrDefault();
        }

        /// <summary>
        /// Gets the player session using a Steam ID
        /// </summary>
        /// <param name="playerId"></param>
        /// <returns></returns>
        public static PlayerSession FindById(string playerId)
        {
            PlayerSession session = null;
            foreach (KeyValuePair<uLink.NetworkPlayer, PlayerSession> s in Sessions)
            {
                if (!playerId.Equals(s.Value.SteamId.ToString()))
                {
                    continue;
                }

                session = s.Value;
                break;
            }
            return session;
        }

        /// <summary>
        /// Gets the player session using a uLink.NetworkPlayer
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
        public PlayerSession Session(uLink.NetworkPlayer player) => GameManager.Instance.GetSession(player);

        /// <summary>
        /// Gets the player session using a UnityEngine.GameObject
        /// </summary>
        /// <param name="go"></param>
        /// <returns></returns>
        public static PlayerSession Session(GameObject go)
        {
            return (from s in Sessions where go.Equals(s.Value.WorldPlayerEntity) select s.Value).FirstOrDefault();
        }

        /// <summary>
        /// Returns all connected sessions
        /// </summary>
        public static Dictionary<uLink.NetworkPlayer, PlayerSession> Sessions => GameManager.Instance.GetSessions();

        #endregion Player Finding
    }
}
