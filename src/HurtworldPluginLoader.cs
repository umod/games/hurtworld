﻿using System;
using uMod.Common;
using uMod.Plugins;

namespace uMod.Game.Hurtworld
{
    /// <summary>
    /// Responsible for loading the core plugin
    /// </summary>
    public class HurtworldPluginLoader : PluginLoader
    {
        public override Type[] CorePlugins => new[] { typeof(Hurtworld) };

        public HurtworldPluginLoader(ILogger logger) : base(logger)
        {
        }
    }
}
